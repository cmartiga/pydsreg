import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pydSreg",
    version="0.0.1",
    author="Carlos Marti-Gomez",
    author_email="cmarti@cnic.es",
    description="Integrated inference of differential splicing and regulation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/cmartiga/pydsreg/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
