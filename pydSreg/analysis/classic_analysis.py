from os.path import join

from statsmodels.stats.multitest import multipletests

import numpy as np
import pandas as pd
from pydSreg.analysis.process_results.analyze_dAS import add_threshold_classes
from pydSreg.settings import SIMULATIONS_DIR, CONFIG_FPATH, RESULTS_DIR
from pydSreg.utils import LogTrack
import statsmodels.api as sm
import statsmodels.formula.api as smf


def padjust(p_values, method='fdr_bh'):
    if p_values.shape[0] == 0:
        return([])
    return(multipletests(p_values, method=method)[1])


def run_enrichment(results, binding_sites, label, class_label='Class.adj'):
    sel_ids = [int(x) for x in results['id']]
    bs = binding_sites[sel_ids, :]
    group = (results[class_label] == label).astype(int)

    for i in range(binding_sites.shape[1]):
        rbp = np.array(bs[:, i]).flatten()
        if group.sum() == 0 or rbp.sum() == 0:
            pvalue = np.nan
            estimate = np.nan
        else:
            data = pd.DataFrame({'group': group, 'rbp': rbp})
            model = smf.glm(formula='group ~ rbp', data=data,
                            family=sm.families.Binomial())
            results = model.fit()
            pvalue = results.pvalues[-1]
            estimate = results.params[-1]

        record = {'rbp': i, 'estimate': estimate, 'pvalue': pvalue,
                  'mode': 'single', 'group': label,
                  'dAS_class_label': class_label}
        yield(record)


def to_df(records):
    df = pd.DataFrame(records)
    sel_rows = np.isnan(df['pvalue']) == False
    df['fdr'] = np.nan
    df.loc[sel_rows, 'fdr'] = padjust(df.loc[sel_rows, 'pvalue'])
    return(df)


if __name__ == '__main__':
    thresholds = [0.01, 0.05, 0.1, 0.2]
    threshold_labels = ['fdr{}'.format(t) for t in thresholds]

    log = LogTrack()
    log.write('Start classical enrichment analyses...')
    config = pd.read_csv(CONFIG_FPATH)
    dfs = []

    for dataset_id in config['dataset_id']:
        log.write('Analysing dataset {}'.format(dataset_id))

        # Load data
        log.write('Loading data...')
        fpath = join(SIMULATIONS_DIR,
                     '{}.binding_sites.csv'.format(dataset_id))
        binding_sites = pd.read_csv(fpath, index_col=0).as_matrix()

        fpath = join(SIMULATIONS_DIR, '{}.dAS.csv'.format(dataset_id))
        results = pd.read_csv(fpath)
        add_threshold_classes(results, thresholds=thresholds)

        # Perform ORA using GLM for each set of events
        for group in ['Included', 'Skipped']:
            for class_label in threshold_labels:
                msg = '\tPerform enrichment analysis for {} with {}'
                log.write(msg.format(group, class_label))
                res = run_enrichment(results, binding_sites,
                                     label=group, class_label=class_label)
                df = to_df(res)
                df['dataset_id'] = dataset_id
                dfs.append(df)
                nans = np.isnan(df['pvalue']).sum()
                if nans > 0:
                    msg = '\t{} tests out of {} could not be calculated'
                    log.write(msg.format(nans, df.shape[0]))

    # Save results
    df = pd.concat(dfs)
    fpath = join(RESULTS_DIR, 'merged_enrichment.csv'.format(dataset_id))
    df.to_csv(fpath)

    log.write('Finished succesfully')
