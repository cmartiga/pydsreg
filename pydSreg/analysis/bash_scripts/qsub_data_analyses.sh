bash get_env.sh

MODELS="model_reg"
MEM=4G

QSUB="qsub -P AG -A cmarti -l h_vmem=$MEM -pe pthreads 4"
QSUB_COMP="qsub -P AG -A cmarti -l h_vmem=4G"


if [ ! -e $LOGS_DIR ]; then
   mkdir $LOGS_DIR
fi

for model in $MODELS
do
    
    ##################################
    # Compile stan model if required #
    ##################################
    COMP_JID=10
    if [[ $COMPILE == 1 ]]; then
        jname=compile.$model
        err=$LOGS_DIR/$jname.err
        out=$LOGS_DIR/$jname.out
        if [ -e $out ]; then
           rm $err $out
        fi
    
        cmd="$ACTIVATE; python $BIN_DIR/compile.py $model"
        COMP_JID=`echo $cmd | $QSUB_COMP -N $jname -e $err -o $out | cut -f 3 -d ' '`
    fi

    ###########################
    # Fit a model per dataset #
    ###########################    
    for event_type in exon_cassette
    do
        for dataset in cm_maturation
        do
            jname=$dataset.$event_type.$model.fit
            err=$LOGS_DIR/$jname.err
            out=$LOGS_DIR/$jname.out
            if [ -e $out ]; then
               rm $err $out
            fi
        
            cmd="$ACTIVATE; python $ANALYSIS_DIR/fit_data.py $event_type $model $dataset"
            echo $cmd | $QSUB -N $jname -e $err -o $out -hold_jid $COMP_JID
        done
    done
done
