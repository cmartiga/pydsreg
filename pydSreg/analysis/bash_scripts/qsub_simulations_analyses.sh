bash get_env.sh

COMPILE=0
MODELS="model_null model_reg"
MEM=3G

QSUB="qsub -P AG -A cmarti -l h_vmem=$MEM -pe pthreads 4"
QSUB_COMP="qsub -P AG -A cmarti -l h_vmem=4G"


#######################
# Run bayesian models #
#######################

for MODEL in $MODELS
do
    ##################################
    # Compile stan model if required #
    ##################################
    
    COMP_JID=10
    if [[ $COMPILE == 1 ]]; then
        jname=compile.$MODEL
        err=$LOGS_DIR/$jname.err
        out=$LOGS_DIR/$jname.out
        if [ -e $out ]; then
           rm $err $out
        fi
    
        cmd="$ACTIVATE; python $BIN_DIR/compile.py $MODEL"
        COMP_JID=`echo $cmd | $QSUB_COMP -N $jname -e $err -o $out | cut -f 3 -d ' '`
    fi
    
    ###########################
    # Fit a model per dataset #
    ###########################
    datasets=`cut -f 1 -d ',' $SIMDIR/config.csv | grep -v dataset_id`
    for dataset in $datasets
    do
        jname=fit.$dataset
        err=$LOGS_DIR/$jname.err
        out=$LOGS_DIR/$jname.out
        if [ -e $out ]; then
           rm $err $out
        fi
    
        cmd="$ACTIVATE; python $ANALYSIS_DIR/fit_simulations.py $dataset $MODEL"
        echo $cmd | $QSUB -N $jname -e $err -o $out -hold_jid $COMP_JID,$SIM_JID
    done
done