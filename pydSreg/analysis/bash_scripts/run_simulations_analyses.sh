bash get_env.sh

MODELS="model_null model_reg"

# Simulate datasets 
python $ANALYSIS_DIR/simulate_datasets.py
datasets=`cut -f 1 -d ',' $SIMDIR/config.csv | grep -v dataset_id`

# Run differential splicing analyses
for i in $dataset
do
    echo "Running dataset $i"
    Rscript $R_DIR/dAS_analysis.R $i $SIMDIR
done

# Run over-representation analysis on significant changes
python $ANALYSIS_DIR/classic_analysis.py

# Run GSEA using GLM estimates
python $ANALYSIS_DIR/gsea.py

# Run bayesian models
for MODEL in $MODELS
do
    # Compile Stan model
    python $BIN_DIR/compile.py $MODEL

    # Fit stan model for each dataset    
    for dataset in $datasets
    do
        python $ANALYSIS_DIR/fit_simulations.py $dataset $MODEL
    done
done

# Merge differential splicing results for all methods
python $PROCESS_DIR/analyze_dAS.py

# Merge results from all methods for enrichment
python $PROCESS_DIR/analyze_enrichment.py
