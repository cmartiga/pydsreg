bash get_env.sh

MODELS="model_reg"
EVENT="exon_cassette"
dataset="cm_maturation"

# Run differential splicing analyses
Rscript $R_DIR/dAS_analysis_data.R $DATA_DIR $EVENT

# Run over-representation analysis on significant changes
python $ANALYSIS_DIR/classic_analysis_data.py

# Run GSEA using GLM estimates
python $ANALYSIS_DIR/gsea_data.py

# Run bayesian models
for MODEL in $MODELS
do
    # Compile Stan model
    python $BIN_DIR/compile.py $MODEL

    # Fit stan model for each dataset    
    python $ANALYSIS_DIR/fit_data.py $EVENT $MODEL $dataset
done
