BASE_DIR../../..
LOGS_DIR=$BASE_DIR/../logs
ACTIVATE="source /data3/cmarti/pystan/bin/activate; export PYTHONPATH=\"/data3/cmarti/pystan:/data3/cmarti/pydsreg\""
ANALYSIS_DIR=..
R_DIR=$ANALYSIS_DIR/Rscripts
DATA_DIR=$BASE_DIR/data
BIN_DIR=$BASE_DIR/pydSreg/bin
PROCESS_DIR=$ANALYSIS_DIR/process_results
SIMDIR=$BASE_DIR/simulations