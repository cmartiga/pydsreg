from os.path import join
import sys

from pydSreg.modeling.models import BayesianModel
from pydSreg.settings import SIMULATIONS_DIR
from pydSreg.utils import load_pickle


if __name__ == '__main__':
    dataset = sys.argv[1]
    model_label = sys.argv[2]

    fpath = join(SIMULATIONS_DIR, '{}.p'.format(dataset))
    data = load_pickle(fpath)
    data['inclusion'] = data['inclusion'].transpose()
    data['total'] = data['total'].transpose()
    data['N'] = data['total'].shape[1]
    data['K'] = data['total'].shape[0]
    data['W'] = data['binding_sites'].shape[1]
    data['mappability'] = 0

    pars = ['sigma', 'alpha', 'beta']
    if model_label != 'model_null':
        pars.append('reg_effects')

    model = BayesianModel()
    model.init(model_label=model_label)
    model.fit(data, pars=pars, n_iter=2000, n_chains=4)
    prefix = join(SIMULATIONS_DIR, '{}.{}'.format(dataset, model_label))
    model.write_fit(prefix)
