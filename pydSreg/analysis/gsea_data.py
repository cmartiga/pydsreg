from os.path import join

import numpy as np
import pandas as pd
from pydSreg.analysis.classic_analysis import to_df
from pydSreg.analysis.gsea import calc_enrichment_scores
from pydSreg.settings import DATA_DIR, RESULTS_DIR
from pydSreg.utils import LogTrack


def calc_null_enrichment_scores(scores, n=10000, log=None):
    if hasattr(scores, 'index'):
        rownames = np.array(scores.index)
    else:
        rownames = np.arange(scores.shape[0])
    es_nulls = [[], []]
    if log is not None:
        log.write('Start permutations for ES null distribution')
    for i in range(n):
        if log is not None and i % 500 == 0:
            log.write('Permutation done: {}'.format(i))
        np.random.shuffle(rownames)
        es_min, es_max = calc_enrichment_scores(rownames, scores)
        es_nulls[0].append(es_min)
        es_nulls[1].append(es_max)
    return(np.array(es_nulls[0]), np.array(es_nulls[1]))


def run_gsea(results, binding_sites, npermutations=10000, log=None):
    sorted_exon_ids = results.sort_values('estimate')['id']

    scores = binding_sites.loc[sorted_exon_ids, :].as_matrix()
    scores = scores - scores.mean(0)
    rownames = np.arange(scores.shape[0])

    if log is not None:
        log.write('Calculating Enrichment scores for data')

    es_min, es_max = calc_enrichment_scores(rownames, scores)
    null_es_min, null_es_max = calc_null_enrichment_scores(scores,
                                                           n=npermutations,
                                                           log=log)
    p = 2 * np.min(np.vstack([np.mean(es_min < null_es_min, 0),
                              np.mean(es_max > null_es_max, 0)]), axis=0)

    for i in range(p.shape[0]):
        if np.abs(es_min[i]) > np.abs(es_max[i]):
            estimate = es_min[i]
        else:
            estimate = es_max[i]

        record = {'rbp': binding_sites.columns[i],
                  'estimate': estimate, 'pvalue': min(p[i], 1)}
        yield(record)


if __name__ == '__main__':
    dataset = 'cm_differentiation'
    log = LogTrack()
    log.write('Start GSEA analysis on the {} dataset...'.format(dataset))
    dfs = []

    for event_type in ['exon_cassette']:
        log.write('Analysing dataset {}'.format(event_type))

        log.write('Loading data...')
        fpath = join(DATA_DIR, '{}.{}.binding_sites.csv'.format(dataset,
                                                                event_type))
        binding_sites = pd.read_csv(fpath, index_col=0)

        fpath = join(DATA_DIR, '{}.{}.dAS.csv'.format(dataset,
                                                      event_type))
        results = pd.read_csv(fpath)

        log.write('\tPerform GSEA')
        res = run_gsea(results, binding_sites, npermutations=20000,
                       log=log)
        df = to_df(res)
        df['event_type'] = event_type
        dfs.append(df)
        nans = np.isnan(df['pvalue']).sum()

        if nans > 0:
            msg = '\t{} tests out of {} could not be calculated'
            log.write(msg.format(nans, df.shape[0]))

        df = pd.concat(dfs)
        fpath = join(RESULTS_DIR, '{}.{}.gsea.csv'.format(dataset, event_type))
        df.to_csv(fpath)
    log.write('Finished succesfully')
