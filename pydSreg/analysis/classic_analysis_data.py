from os.path import join

import numpy as np
import pandas as pd
from pydSreg.analysis.classic_analysis import to_df
from pydSreg.settings import DATA_DIR, RESULTS_DIR
from pydSreg.utils import LogTrack
import statsmodels.api as sm
import statsmodels.formula.api as smf


def run_enrichment(results, binding_sites, label, class_label='Class.adj'):
    bs = binding_sites.loc[results['id'], :]
    group = (results[class_label] == label).astype(int)

    for rbp_name in binding_sites.columns:
        rbp = np.array(bs[rbp_name]).flatten()
        if group.sum() == 0 or rbp.sum() == 0:
            pvalue = np.nan
            estimate = np.nan
        else:
            data = pd.DataFrame({'group': group, 'rbp': rbp})
            model = smf.glm(formula='group ~ rbp', data=data,
                            family=sm.families.Binomial())
            results = model.fit()
            pvalue = results.pvalues[-1]
            estimate = results.params[-1]

        record = {'rbp': rbp_name, 'estimate': estimate, 'pvalue': pvalue,
                  'mode': 'single', 'group': label,
                  'dAS_class_label': class_label}
        yield(record)


if __name__ == '__main__':
    log = LogTrack()
    log.write('Start classical enrichment analyses...')
    dfs = []
    dataset = 'cm_maturation'

    for event_type in ['exon_cassette']:
        log.write('Analysing dataset {}'.format(event_type))

        log.write('Loading data...')
        fpath = join(DATA_DIR, '{}.{}.binding_sites.csv'.format(dataset,
                                                                event_type))
        binding_sites = pd.read_csv(fpath, index_col=0)

        fpath = join(DATA_DIR, '{}.{}.dAS.csv'.format(dataset,
                                                      event_type))
        results = pd.read_csv(fpath)

        for group in ['Included', 'Skipped']:
            for class_label in ['Class.adj', 'Class.raw']:
                msg = '\tPerform enrichment analysis for {} with {}'
                log.write(msg.format(group, class_label))
                res = run_enrichment(results, binding_sites,
                                     label=group, class_label=class_label)
                df = to_df(res)
                df['event_type'] = event_type
                dfs.append(df)
                nans = np.isnan(df['pvalue']).sum()
                if nans > 0:
                    msg = '\t{} tests out of {} could not be calculated'
                    log.write(msg.format(nans, df.shape[0]))

        df = pd.concat(dfs)
        fpath = join(RESULTS_DIR, '{}.{}.enrichment.csv'.format(dataset,
                                                                event_type))
        df.to_csv(fpath)
    log.write('Finished succesfully')
