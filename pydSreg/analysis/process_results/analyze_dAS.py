from csv import DictWriter
from os.path import join, exists

from sklearn.metrics.classification import f1_score, precision_score, \
    recall_score
from sklearn.metrics.ranking import roc_auc_score, roc_curve

import numpy as np
import pandas as pd
from pydSreg.settings import CONFIG_FPATH, SIMULATIONS_DIR, RESULTS_DIR
from pydSreg.utils import LogTrack, calc_calibration


def add_threshold_classes(results, thresholds=[0.2, 0.1, 0.05, 0.01]):
    for threshold in thresholds:
        fieldname = 'fdr{}'.format(threshold)
        results[fieldname] = 'No-change'
        results.loc[np.logical_and(results['fdr'] < threshold,
                                   results['estimate'] > 0), fieldname] = 'Included'
        results.loc[np.logical_and(results['fdr'] < threshold,
                                   results['estimate'] < 0), fieldname] = 'Skipped'


def load_dataset_data(dataset_id):
    fpath = join(SIMULATIONS_DIR, '{}.beta.csv'.format(dataset_id))
    reg_effects = np.array(pd.read_csv(fpath, index_col=0).iloc[:, 0])

    fpath = join(SIMULATIONS_DIR, '{}.dAS.csv'.format(dataset_id))
    results = pd.read_csv(fpath)
    add_threshold_classes(results)
    return(reg_effects, results)


def parse_model_results(dataset_id, suffix='', model='model',
                        n_events=2000):
    if suffix == '_null':
        fpath = join(SIMULATIONS_DIR,
                     '{}.model_null.csv'.format(dataset_id))
    else:
        fpath = join(SIMULATIONS_DIR,
                     '{}.results{}.csv'.format(dataset_id, suffix))
    if not exists(fpath):
        return(None)
    sel_cols = ['beta.{}'.format(i) for i in range(1, n_events + 1)]
    posterior = pd.read_csv(fpath, usecols=sel_cols)
    colnames = posterior.columns
    p = np.vstack([(posterior > 0).mean(0), (posterior < 0).mean(0)])
    p = np.min(p, axis=0)
    p = 2 * p
    event = [int(name.split('.')[-1]) - 1 for name in colnames]

    data = pd.DataFrame({'p': p, model: 'No-change', 'id': event,
                         'beta': posterior.mean(0)})
    data.loc[np.logical_and(data['p'] < 0.05,
                            data['beta'] > 0), model] = 'Included'
    data.loc[np.logical_and(data['p'] < 0.05,
                            data['beta'] < 0), model] = 'Skipped'

    ci = np.percentile(posterior, q=[2.5, 97.5], axis=0)
    return(data, ci)


def calc_metrics(results, betas, class_label='Class.adj', ci=None):
    sel_idxs = [int(x) for x in results['id']]
    betas = betas[sel_idxs]
    targets = (betas != 0).astype(int)
    ypred = (results[class_label] != 'No-change').astype(int)

    # Select variable for ranking for AUROC calculation
    if 'fdr' in class_label:
        label = 'fdr'
    elif class_label in ('Null model', 'dSreg'):
        label = 'p'
    else:
        msg = 'Class label must have fdr substring or Null model or dSreg'
        raise ValueError(msg)

    # Calcuulate evaluation metrics
    scores = 1 - results[label]
    calibration = calc_calibration(ci, betas)
    record = {'specificity': np.logical_and(ypred == targets,
                                            ypred == 0).sum() / (1 - targets).sum(),
              'f1': f1_score(targets, ypred),
              'roc_auc': roc_auc_score(targets, scores),
              'precision': precision_score(targets, ypred),
              'recall': recall_score(targets, ypred),
              'class_label': class_label,
              'calibration': calibration}

    fpr, tpr, _ = roc_curve(targets, scores)
    roc = pd.DataFrame({'tpr': tpr, 'fpr': fpr, 'class_label': class_label})
    return(record, roc)


if __name__ == '__main__':
    thresholds = [0.01, 0.05, 0.1, 0.2]
    threshold_labels = ['fdr{}'.format(t) for t in thresholds]

    log = LogTrack()
    log.write('Start processing differential AS results...')
    config = pd.read_csv(CONFIG_FPATH).set_index('dataset_id').to_dict(orient='index')

    fpath = join(RESULTS_DIR, 'dAS.results.csv')
    rocs = []
    with open(fpath, 'w') as fhand:
        fieldnames = ['dataset_id', 'recall', 'precision', 'specificity',
                      'f1', 'roc_auc', 'class_label', 'loglambda',
                      'calibration']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()

        for dataset_id, dataset_data in config.items():
            log.write('Load dataset {}'.format(dataset_id))

            # Load dataset results and real values
            beta, results = load_dataset_data(dataset_id)
            n_events = config[dataset_id]['n_events']

            # Evaluate for different significance thresholds
            for class_label in threshold_labels:
                metrics, roc = calc_metrics(results, beta,
                                            class_label=class_label)
                metrics['dataset_id'] = dataset_id
                metrics['loglambda'] = dataset_data['loglambda']
                writer.writerow(metrics)

            # Calculate threshold independent ROC curves
            roc['class_label'] = 'GLM'
            roc['loglambda'] = dataset_data['loglambda']
            roc['dataset_id'] = dataset_id
            rocs.append(roc)

            # Evaluate bayesian models
            for model, suffix in [('Null model', '_null'),
                                  ('dSreg', '_reg')]:
                mod_dataset, ci = parse_model_results(dataset_id, model=model,
                                                      suffix=suffix,
                                                      n_events=n_events)
                metrics, roc = calc_metrics(mod_dataset, beta,
                                            class_label=model, ci=ci)
                metrics['dataset_id'] = dataset_id
                metrics['loglambda'] = config[dataset_id]['loglambda']
                writer.writerow(metrics)

                roc['loglambda'] = config[dataset_id]['loglambda']
                roc['dataset_id'] = dataset_id
                rocs.append(roc)

    # Save ROC curves
    roc = pd.concat(rocs)
    fpath = join(RESULTS_DIR, 'dAS.roc_curves.csv')
    roc.to_csv(fpath)

    log.write('Finished succesfully')
