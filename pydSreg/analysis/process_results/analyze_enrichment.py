from csv import DictWriter
from os.path import join, exists

from sklearn.metrics.classification import (f1_score, precision_score,
                                            recall_score)
from sklearn.metrics.ranking import roc_auc_score, roc_curve

import numpy as np
import pandas as pd
from pydSreg.settings import CONFIG_FPATH, SIMULATIONS_DIR, RESULTS_DIR
from pydSreg.utils import LogTrack, calc_calibration


def load_dataset_targets(dataset_id):
    fpath = join(SIMULATIONS_DIR, '{}.reg_effect.csv'.format(dataset_id))
    data = np.array(pd.read_csv(fpath, index_col=0).iloc[:, 0])
    return(data)


def parse_model_results(dataset_id, suffix='', model='model',
                        n_regulators=50):
    fpath = join(SIMULATIONS_DIR,
                 '{}.results{}.csv'.format(dataset_id, suffix))
    if not exists(fpath):
        return(None)
    sel_cols = ['reg_effects.{}'.format(i) for i in range(1, n_regulators + 1)]
    posterior = pd.read_csv(fpath, usecols=sel_cols)
    colnames = posterior.columns
    p = np.vstack([(posterior > 0).mean(0), (posterior < 0).mean(0)])
    p = np.min(p, axis=0)
    p = 2 * p
    rbp = [int(name.split('.')[-1]) - 1 for name in colnames]
    data = pd.DataFrame({'p': p, model: (p < 0.01).astype(int), 'rbp': rbp})
    data['dAS_class_label'] = 'Class.raw'
    ci = np.percentile(posterior, q=[2.5, 97.5], axis=0)
    return(data, ci)


def calc_metrics(results, reg_effects, ora_threshold='Class.adj',
                 das_threshold=None, ci=None):
    if 'fdr' in ora_threshold:
        score_for_auroc = 'fdr'
    elif ora_threshold in ('Null model', 'dSreg'):
        score_for_auroc = 'p'
    else:
        raise ValueError('Incorrect label: {}'.format(ora_threshold))

    # Filter according to the threshold used for differential splicing
    if das_threshold is not None:
        results = results.loc[results['dAS_class_label'] == das_threshold, :]

    # Select entry with higher significance when testing included and skipped
    # separately
    results = results.groupby(['rbp'])[score_for_auroc,
                                       ora_threshold].min().reset_index()
    sel_idxs = [int(x) for x in results['rbp']]

    # Calculate evaluation metrics
    reg_effects = reg_effects[sel_idxs]
    targets = (reg_effects != 0).astype(int)
    ypred = results[ora_threshold]
    scores = 1 - results[score_for_auroc]
#     if ora_threshold == 'fdr0.05':
#         print(ypred.sum())
#         print(np.array(targets), np.array(ypred), recall_score(targets, ypred))
#         input('continue')
    calibration = calc_calibration(ci, reg_effects)
    specificity = np.logical_and(ypred == targets,
                                 ypred == 0).sum() / (1 - targets).sum()
    record = {'specificity': specificity,
              'f1': f1_score(targets, ypred),
              'roc_auc': roc_auc_score(targets, scores),
              'precision': precision_score(targets, ypred),
              'recall': recall_score(targets, ypred),
              'class_label': ora_threshold,
              'dAS_class_label': das_threshold,
              'calibration': calibration}

    fpr, tpr, _ = roc_curve(targets, scores)
    roc = pd.DataFrame({'tpr': tpr, 'fpr': fpr,
                        'class_label': ora_threshold,
                        'dAS_class_label': das_threshold})
    return(record, roc)


if __name__ == '__main__':
    thresholds = [0.05, 0.2]
    threshold_labels = ['fdr{}'.format(t) for t in thresholds]

    log = LogTrack()
    log.write('Start processing enrichment results...')

    # Load data
    config = pd.read_csv(CONFIG_FPATH).set_index('dataset_id')
    config = config.to_dict(orient='index')

    fpath = join(RESULTS_DIR, 'merged_enrichment.csv')
    enrichment = pd.read_csv(fpath).fillna(1)
    enrichment['ci'] = None

    fpath = join(RESULTS_DIR, 'merged_gsea.csv')
    gsea = pd.read_csv(fpath)
    gsea['ci'] = None

    # Create classes for each threshold
    for threshold in thresholds:
        for df in [gsea, enrichment]:
            df['fdr{}'.format(threshold)] = (df['fdr'] < threshold).astype(int)

    # Load enrichment results and analyze results
    fpath = join(RESULTS_DIR, 'enrichment.results.csv')
    rocs = []
    with open(fpath, 'w') as fhand:

        # Define metrics to use
        fieldnames = ['dataset_id', 'recall', 'precision', 'specificity',
                      'f1', 'roc_auc', 'class_label', 'loglambda',
                      'dAS_class_label', 'calibration']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()

        for dataset_id, results in enrichment.groupby(['dataset_id']):
            log.write('Load dataset {}'.format(dataset_id))

            # Get real values used in simulations
            reg_effects = load_dataset_targets(dataset_id)
            n_regulators = config[dataset_id]['n_regulators']

            # Evaluate ORA results
            for ora_threshold in threshold_labels:
                for das_threshold in threshold_labels:
                    metrics, roc = calc_metrics(results, reg_effects,
                                                ora_threshold=ora_threshold,
                                                das_threshold=das_threshold)
                    metrics['dataset_id'] = dataset_id
                    metrics['loglambda'] = config[dataset_id]['loglambda']
                    writer.writerow(metrics)

                # Store roce curves
                roc['class_label'] = 'ORA'
                roc['loglambda'] = config[dataset_id]['loglambda']
                roc['dataset_id'] = dataset_id
                rocs.append(roc)

            # Parse GSEA results
            dataset_gsea = gsea.loc[gsea['dataset_id'] == dataset_id, :]
            metrics, roc = calc_metrics(dataset_gsea, reg_effects,
                                        ora_threshold='fdr0.05')
            metrics['dataset_id'] = dataset_id
            metrics['loglambda'] = config[dataset_id]['loglambda']
            metrics['class_label'] = 'GSEA'
            writer.writerow(metrics)

            roc['loglambda'] = config[dataset_id]['loglambda']
            roc['dataset_id'] = dataset_id
            roc['class_label'] = 'GSEA'
            rocs.append(roc)

            # Load bayesian model results
            model = 'dSreg'
            mod_dset, ci = parse_model_results(dataset_id, model=model,
                                               suffix='_reg',
                                               n_regulators=n_regulators)
            metrics, roc = calc_metrics(mod_dset, reg_effects,
                                        ora_threshold=model, ci=ci)
            metrics['dataset_id'] = dataset_id
            metrics['loglambda'] = config[dataset_id]['loglambda']
            writer.writerow(metrics)

            roc['loglambda'] = config[dataset_id]['loglambda']
            roc['dataset_id'] = dataset_id
            rocs.append(roc)

    roc = pd.concat(rocs)
    fpath = join(RESULTS_DIR, 'enrichment.roc_curves.csv')
    roc.to_csv(fpath)

    log.write('Finished succesfully')
