from csv import DictWriter
from os.path import join

from numpy.linalg.linalg import cholesky, LinAlgError
from scipy.stats._multivariate import invwishart

import numpy as np
import pandas as pd
from pydSreg.settings import SIMULATIONS_DIR, CONFIG_FPATH
from pydSreg.utils import LogTrack, write_pickle


def simulate_corr_matrix(size):
    corr = np.ones((size, size))
    for i in range(size):
        for j in range(i + 1, size):
            corr[i, j] = np.random.beta(2, 5)
            corr[j, i] = corr[i, j]
    return(corr)


def simulate_scale_matrix(size, n, sigma):
    scale = np.ones((size, size)) * sigma
    for i in range(size):
        for j in range(i + 1, size):
            scale[i, j] = 1 / n * sigma
            scale[j, i] = 1 / n * sigma
    return(scale)


def _simulate_sites_matrix(n_events, n_regulators, sigma=1, K=2,
                           logitmean=-2.5):
    scale = simulate_scale_matrix(size=n_regulators, n=n_regulators + 1,
                                  sigma=sigma)
    covariance = invwishart.rvs(n_regulators + 1, scale=scale)
    L = cholesky(covariance)
    X_tilde = np.random.normal(0, 1, size=(n_regulators, n_events))
    X_mean = np.random.normal(logitmean, 1, size=(n_regulators, 1))
    X = np.transpose(X_mean + np.dot(L, X_tilde))
    p = np.exp(X) / (1 + np.exp(X))
    u = np.random.uniform(size=(n_events, n_regulators))
    return((u < p).astype(int))


def simulate_sites_matrix(n_events, n_regulators, sigma=1, K=2,
                          logitmean=-2.5, **kwargs):
    try:
        return(_simulate_sites_matrix(n_events, n_regulators,
                                      sigma=sigma, K=K, logitmean=logitmean))
    except LinAlgError:
        return(simulate_sites_matrix(n_events, n_regulators, sigma=sigma, K=K,
                                     logitmean=logitmean))


def simulate_baseline_psi(n_events, p_alt=0.2):
    n_alt = int(n_events * p_alt)
    psi = np.append(np.random.uniform(size=(1, n_alt)),
                    np.random.beta(10, 1, size=(1, n_events - n_alt)))
    return(psi)


def simulate_AS_data(binding_sites, n_samples, n_active_reg, p_alt=0.2,
                     beta_sigma=0.1, sample_sigma=0.5, loglambda=3, **kwargs):
    # Simulate PSI distribution from mixture of uniform for AS and beta for CS
    psi = simulate_baseline_psi(binding_sites.shape[0], p_alt)

    # Draw relatively low regulatory effects from uniform
    reg_effects = np.random.uniform(-2.5, 2.5,
                                    size=(binding_sites.shape[1], 1))
    sel_idxs = np.random.choice(np.arange(reg_effects.shape[0]),
                                size=binding_sites.shape[1] - n_active_reg,
                                replace=False)
    reg_effects[sel_idxs] = 0
    alpha = np.log(psi / (1 - psi))
    beta = np.dot(binding_sites, reg_effects).flatten()
    targets = beta != 0
    n_targets = targets.sum()
    beta[targets] += np.random.normal(loc=0, scale=beta_sigma, size=n_targets)
    targets = targets.astype(int)

    # Compose sample-exon probabilities of inclusion
    X1 = np.vstack([alpha] * n_samples).transpose()
    X1 += np.random.normal(loc=0, scale=sample_sigma,
                           size=(alpha.shape[0], n_samples))
    X2 = np.vstack([alpha + beta] * n_samples).transpose()
    X2 += np.random.normal(loc=0, scale=sample_sigma,
                           size=(alpha.shape[0], n_samples))
    X = np.append(X1, X2, axis=1)
    psi = np.exp(X) / (1 + np.exp(X))

    # Sample total event coverage and inclusion reads
    total = np.random.poisson(np.exp(loglambda),
                              size=(binding_sites.shape[0], n_samples * 2))
    inclusion = np.random.binomial(total, psi)
    design = np.array([0] * n_samples + [1] * n_samples)
    data = {'binding_sites': binding_sites, 'inclusion': inclusion,
            'total': total, 'reg_effect': reg_effects, 'targets': targets,
            'design': design, 'beta': beta}
    return(data)


if __name__ == '__main__':
    np.random.seed(0)
    defconfig = {'n_events': 2000, 'n_regulators': 50, 'n_samples': 3,
                 'K': 1, 'p_alt': 0.2, 'loglambda': 2, 'sigma': 0.2,
                 'n_active_reg': 5, 'beta_sigma': 0.1,
                 'sample_sigma': 0.3}
    grid_values = {'loglambda': np.linspace(1, 5.5, 10),
                   'n_regulators': [25, 50, 75, 100, 150, 200, 250]}
    # Hard-coded to keep the order of new simulations with the seed
    params = ['loglambda', 'n_regulators']
    n_datasets = 20

    log = LogTrack()
    log.write('Start dataset simulation')
    log.write('Default configuration:')
    for param, value in defconfig.items():
        log.write('\t\t{}: {}'.format(param, value), add_time=False)

    with open(CONFIG_FPATH, 'w') as fhand:
        fieldnames = ['dataset_id', 'param'] + list(defconfig.keys())
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()
        dataset_id = 0

        for param in params:
            values = grid_values[param]
            for value in values:
                log.write('Simulating data for {}={}...'.format(param, value))
                config = defconfig.copy()
                config[param] = value
                config['param'] = param

                for _ in range(n_datasets):
                    m = simulate_sites_matrix(**config)
                    data = simulate_AS_data(m, **config)
                    dataset_id += 1
                    record = config.copy()
                    record['dataset_id'] = dataset_id
                    writer.writerow(record)

                    dataset_fpath = join(SIMULATIONS_DIR,
                                         '{}.p'.format(dataset_id))
                    write_pickle(data, dataset_fpath)

                    for label in ['inclusion', 'total', 'binding_sites',
                                  'targets', 'beta', 'reg_effect']:
                        fpath = join(SIMULATIONS_DIR,
                                     '{}.{}.csv'.format(dataset_id, label))
                        pd.DataFrame(data[label]).to_csv(fpath)

    log.write('Finished succesfully')
