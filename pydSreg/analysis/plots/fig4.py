from os.path import join, exists

from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pydSreg.analysis.fit_data import load_samples_runs
from pydSreg.plot_utils import add_panel_label, arrange_plot
from pydSreg.settings import SIMULATIONS_DIR, PLOTS_DIR, DATA_DIR, RESULTS_DIR
from pydSreg.utils import add_rbp_and_region_cols
import seaborn as sns


def load_enrichment(dataset, event_type_label):
    fpath = join(RESULTS_DIR, '{}.{}.enrichment.csv'.format(dataset,
                                                            event_type_label))
    enrichment = pd.read_csv(fpath)
    add_rbp_and_region_cols(enrichment)
    return(enrichment)


def load_gsea(dataset, event_type_label):
    fpath = join(RESULTS_DIR,
                 '{}.{}.gsea.csv'.format(dataset, event_type_label))
    gsea = pd.read_csv(fpath)
    add_rbp_and_region_cols(gsea)
    gsea['logpval'] = np.log10(gsea['pvalue']) * np.sign(gsea['estimate'])
    return(gsea)


def load_fit_data(dataset, event_type_label):
    fpath = join(DATA_DIR, '{}.{}.binding_sites.csv'.format(dataset,
                                                            event_type_label))
    binding_sites = pd.read_csv(fpath, index_col=0)

    fpath = join(SIMULATIONS_DIR,
                 '{}.{}.model_reg.csv'.format(dataset, event_type_label))
    fpath2 = join(RESULTS_DIR, '{}.{}.model_reg.reduced.csv')
    fpath2 = fpath2.format(dataset, event_type_label)

    # To enable plotting with only the reduced posterior file with
    # regulatory elements
    if exists(fpath):
        sel_cols = ['reg_effects.{}'.format(i + 1)
                    for i in range(binding_sites.shape[1])]
        fit_data = pd.read_csv(fpath, usecols=sel_cols)
        fit_data.to_csv(fpath2)
    else:
        fit_data = pd.read_csv(fpath2, index_col=0)

    idx = [int(x.split('.')[-1]) - 1 for x in fit_data.columns]
    colnames = binding_sites.columns[idx]
    fit_data.columns = colnames
    sel_cols = np.logical_or(fit_data.quantile(q=0.025, axis=0) > 0,
                             fit_data.quantile(q=0.975, axis=0) < 0)
    sel_rbps = fit_data.columns[sel_cols]

    fit_data = pd.melt(fit_data)
    add_rbp_and_region_cols(fit_data, fieldname='variable')
    return(fit_data, colnames, sel_rbps)


if __name__ == '__main__':
    bs_fpath = join(DATA_DIR)

    event_type = 'Exon cassette'
    dataset = 'cardiac_precursors'
    dataset = 'cm_maturation'

    samples_runs = None
    if dataset == 'cm_maturation':
        fpath = join(DATA_DIR, dataset)
        samples_runs = load_samples_runs(fpath)

    event_type_label = event_type.replace(' ', '_').lower()

    # Load data
    enrichment = load_enrichment(dataset, event_type_label)
    enrichment = enrichment.loc[enrichment['dAS_class_label'] == 'Class.raw', :]

    included = enrichment.loc[enrichment['group'] == 'Included', :]
    skipped = enrichment.loc[enrichment['group'] == 'Skipped', :]
    included['logpval'] = -np.log10(included['fdr'])
    skipped['logpval'] = np.log10(skipped['fdr'])

    gsea = load_gsea(dataset, event_type_label)

    fit_data, rbps, sel_rbps = load_fit_data(dataset, event_type_label)
    means = fit_data.groupby(['variable'])['value'].mean().sort_values()
    sorted_rbps = list(means.index)

    # Figure
    sns.set(style="white")
    fig = plt.figure(figsize=(10, 7))
    gs = GridSpec(85, 76)

    # Plot results from classical analyses
    ax = fig.add_subplot(gs[:, :22])

    sns.barplot(y='rbp', x='logpval', data=included, color='blue',
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    sns.barplot(y='rbp', x='logpval', data=skipped, color='red',
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    ax.set_yticklabels([])
    ax.set_xticks([-20, -10, 0, 10])
    ax.set_xticklabels(['20', '10', '0', '10'])
    ylims = ax.get_ylim()
    ax.plot((0, 0), ylims, linewidth=1.5, color='black')
    arrange_plot(ax, ylabel=r'RBP-region',
                 xlabel=r'$-log_{10}(FDR)$',
                 showlegend=False, vline=[-2, 2], despine=True, despine_left=True,
                 title='ORA')
    add_panel_label(ax, 'A', xfactor=0.2, yfactor=0.05)

    # Plot results from GSEA
    ax = fig.add_subplot(gs[:, 27:49])

    sns.barplot(y='rbp', x='logpval', data=gsea,
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    ax.set_yticklabels([])
    ax.set_xticks([-2, 0, 2])
    ax.set_xticklabels(['2', '0', '2'])
    ylims = ax.get_ylim()
    ax.plot((0, 0), ylims, linewidth=1.5, color='black')
    arrange_plot(ax, ylabel='', title='GSEA',
                 xlabel=r'$-log_{10}(p-value)$',
                 showlegend=False, vline=[-2, 2], despine=True, despine_left=True)
    add_panel_label(ax, 'B', xfactor=0.2, yfactor=0.05)

    # Plot model results
    ax = fig.add_subplot(gs[:, 54:76])

    sns.boxplot(y='variable', x='value', data=fit_data, showfliers=False,
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    ax.set_yticklabels([])
    arrange_plot(ax, ylabel='', title='dSreg',
                 xlabel=r'Regulatory effect $\theta_{j}$',
                 showlegend=False, vline=0, despine=True, despine_left=True)
    add_panel_label(ax, 'C', xfactor=0.2, yfactor=0.05)

    # Save figure
    fpath = join(PLOTS_DIR, 'figure4.pdf')
    fig.savefig(fpath, format='pdf', dpi=240)

    fpath = join(PLOTS_DIR, 'figure4.png')
    fig.savefig(fpath, format='png', dpi=100)

    # Store data
    gsea.set_index('rbp', inplace=True)
    included.set_index('rbp', inplace=True)
    skipped.set_index('rbp', inplace=True)

    df = pd.DataFrame({'rbp': sel_rbps,
                       'Included': included.loc[sel_rbps, 'fdr'],
                       'Skipped': skipped.loc[sel_rbps, 'fdr'],
                       'GSEA': gsea.loc[sel_rbps, 'pvalue'],
                       'dSreg': means.loc[sel_rbps]})
    df.to_csv(join(DATA_DIR, 'HS.results.merged.csv'))
    add_rbp_and_region_cols(df)
