from os.path import join

from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pydSreg.plot_utils import (replace_class_labels, CLASS_COLORS,
                                add_panel_label, arrange_plot,
                                TYPES_COLORS)
from pydSreg.settings import RESULTS_DIR, PLOTS_DIR, CONFIG_FPATH
import seaborn as sns


if __name__ == '__main__':
    config = pd.read_csv(CONFIG_FPATH)
    regs = config.set_index('dataset_id')['n_regulators'].to_dict()

    fpath = join(RESULTS_DIR, 'dAS.results.csv')
    events = pd.read_csv(fpath)
    events['n_regulators'] = [regs[dataset]
                              for dataset in events['dataset_id']]
    events['Type'] = 'Event'
    events1 = events.loc[events['dataset_id'] <= 200, :]
    events2 = events.loc[events['dataset_id'] > 200, :]

    fpath = join(RESULTS_DIR, 'enrichment.results.csv')
    results = pd.read_csv(fpath)
    sel_label = 'class_label'
    filter_label = 'dAS_class_label'
    filter_label_type = 'fdr0.2'

    sel_rows = np.logical_or(results[filter_label] == filter_label_type,
                             [x in ('GSEA', 'dSreg')
                              for x in results[sel_label]])
    results = results.loc[sel_rows, :]
    results['n_regulators'] = [regs[dataset]
                               for dataset in results['dataset_id']]
    results['Type'] = 'Regulator'
    replace_class_labels(results[sel_label])

    results1 = results.loc[results['dataset_id'] <= 200, :]
    results2 = results.loc[results['dataset_id'] > 200, :]

    merged1 = pd.concat([results1, events1])
    merged2 = pd.concat([results2, events2])

    # Figure
    sns.set(style="white")
    fig = plt.figure(figsize=(8, 6))
    gs = GridSpec(90, 60)

    # Plot F1 vs N regulators
    hue_order = ['FDR<0.05', 'FDR<0.2', 'GSEA', 'dSreg']
    axes = fig.add_subplot(gs[:38, 35:60])
    sns.barplot(x='n_regulators', y='f1', hue=sel_label, data=results2,
                hue_order=hue_order, palette=CLASS_COLORS, n_boot=0,
                ax=axes, lw=0.5, edgecolor='white', saturation=1.5)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel='Number of regulators', ylabel='F1 score',
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'B', xfactor=0.25)

    # Plot AUROC vs loglambda
    axes = fig.add_subplot(gs[:38, :25])
    sns.barplot(x='loglambda', y='f1', hue=sel_label, data=results1,
                hue_order=hue_order, palette=CLASS_COLORS, n_boot=0,
                ax=axes, lw=0.5, edgecolor='white', saturation=1.5)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='F1 score',
                 showlegend=False, ticklabels_size=8)
    axes.legend(loc=(0, 1.05), frameon=True, fancybox=True, ncol=2,
                fontsize=9)
    add_panel_label(axes, 'A', xfactor=0.3)

    # Plot calibration for events
    ylims = (0.6, 1)
    axes = fig.add_subplot(gs[52:90, :25])
    sns.boxplot(x='loglambda', y='calibration', hue='Type', data=merged1,
                palette=TYPES_COLORS, showfliers=False, ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$',
                 ylabel='Calibration',
                 showlegend=False, ticklabels_size=8,
                 hline=0.95, ylims=ylims)
    axes.legend(loc=4, frameon=True, fancybox=True)
    add_panel_label(axes, 'C', xfactor=0.3)

    # Plot calibration for events
    axes = fig.add_subplot(gs[52:90, 35:60])
    sns.boxplot(x='n_regulators', y='calibration', hue='Type', data=merged2,
                palette=TYPES_COLORS, showfliers=False, ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel='Number of regulators',
                 ylabel='Calibration',
                 showlegend=False, ticklabels_size=8,
                 hline=0.95, ylims=ylims)
    add_panel_label(axes, 'D', xfactor=0.25)

    # Save figure
    fpath = join(PLOTS_DIR, 'figure3.pdf')
    fig.savefig(fpath, format='pdf', dpi=240)

    fpath = join(PLOTS_DIR, 'figure3.png')
    fig.savefig(fpath, format='png', dpi=100)
