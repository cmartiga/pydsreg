from os.path import join

from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import pandas as pd
from pydSreg.plot_utils import (replace_class_labels, CLASS_COLORS,
                                add_panel_label, arrange_plot,
                                get_panel_subplots)
from pydSreg.settings import RESULTS_DIR, PLOTS_DIR
import seaborn as sns


if __name__ == '__main__':
    fpath = join(RESULTS_DIR, 'dAS.results.csv')
    results = pd.read_csv(fpath)

    # Filter results from coverage exploration
    results = results.loc[results['dataset_id'] <= 200, :]
    datasets = set(results['dataset_id'])
    replace_class_labels(results['class_label'])

    fpath = join(RESULTS_DIR, 'dAS.roc_curves.csv')
    roc = pd.read_csv(fpath, index_col=0)
    sel_rows = [dataset in datasets for dataset in roc['dataset_id']]
    roc = roc.loc[sel_rows, :]
    replace_class_labels(roc['class_label'])

    # Figure
    hue_order = ['FDR<0.2', 'FDR<0.05', 'Null model', 'dSreg']

    sns.set(style="white")
    fig = plt.figure(figsize=(14, 8))
    gs = GridSpec(100, 100)
    linewidth = 1.2
    saturation = 1.5

    # Plot sensitivity
    axes = fig.add_subplot(gs[:40, :25])
    sns.boxplot(x='loglambda', y='recall', hue='class_label', data=results,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes, linewidth=linewidth, saturation=saturation)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='Sensitivity',
                 showlegend=True, legend_frame=False, ticklabels_size=8)
    axes.legend(loc=(0, 1.05), frameon=True, fancybox=True, ncol=2, fontsize=8)
    add_panel_label(axes, 'A', xfactor=0.3)

    # Plot specificity
    axes = fig.add_subplot(gs[:40, 35:60])
    sns.boxplot(x='loglambda', y='specificity', hue='class_label', data=results,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes, linewidth=linewidth, saturation=saturation)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='Specificity',
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'B', xfactor=0.3)

    # Plot F1
    axes = fig.add_subplot(gs[:40, 70:])
    sns.boxplot(x='loglambda', y='f1', hue='class_label', data=results,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes, linewidth=1, saturation=saturation)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='F1 score',
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'C', xfactor=0.25)

    # Plot ROC curves
    hue_order = ['GLM', 'Null model', 'dSreg']
    subplots = get_panel_subplots(fig, gs, nrow=2, ncol=5, xs=[0, 60],
                                  ys=[55, 100], yspace=4, xspace=2)
    for i, (loglambda, subdata) in enumerate(roc.groupby(['loglambda'])):
        axes = subplots[i]
        for class_label, df in subdata.groupby(['class_label']):
            for _, dataset_roc in df.groupby(['dataset_id']):
                axes.plot(dataset_roc['fpr'], dataset_roc['tpr'],
                          c=CLASS_COLORS[class_label], lw=0.5, alpha=0.3)

        ylabel = 'True Positive Rate'
        xlabel = ''
        if i == 7:
            xlabel = 'False Positive Rate'
        if i < 5:
            axes.set_xticklabels([])
        if i % 5 != 0:
            ylabel = ''
            axes.set_yticklabels([])
        arrange_plot(axes, xlims=(0, 1), ylims=(0, 1),
                     xlabel=xlabel, ylabel=ylabel, ticklabels_size=7,
                     fontsize=9, despine=False)
        axes.set_title(r'$log(\lambda)$ = {}'.format(loglambda), fontsize=7)
#         sns.despine(ax=axes)
    add_panel_label(subplots[0], 'D', xfactor=0.78, yfactor=0.2)

    # Plot ROC AUC
    hue_order = ['GLM', 'Null model', 'dSreg']
    results['class_label'].replace('FDR<0.05', 'GLM', inplace=True)
    axes = fig.add_subplot(gs[55:, 70:])
    sns.boxplot(x='loglambda', y='roc_auc', hue='class_label', data=results,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes, linewidth=linewidth, saturation=saturation)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='ROC-AUC',
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'E', xfactor=0.25, yfactor=0.1)

    # Save figure
    fpath = join(PLOTS_DIR, 'figure2.pdf')
    fig.savefig(fpath, format='pdf', dpi=240)

    fpath = join(PLOTS_DIR, 'figure2.png')
    fig.savefig(fpath, format='png', dpi=100)
