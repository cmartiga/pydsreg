from os.path import join

import numpy as np
import pandas as pd
from pydSreg.analysis.classic_analysis import to_df
from pydSreg.settings import (SIMULATIONS_DIR, CONFIG_FPATH, RESULTS_DIR)
from pydSreg.utils import LogTrack


def calc_enrichment_scores(sorted_idxs, scores):
    s = np.cumsum(scores[sorted_idxs, :], axis=0)
    return(s.min(0), s.max(0))


def calc_null_enrichment_scores(scores, n=10000):
    rownames = np.arange(scores.shape[0])
    es_nulls = [[], []]
    for _ in range(n):
        np.random.shuffle(rownames)
        es_min, es_max = calc_enrichment_scores(rownames, scores)
        es_nulls[0].append(es_min)
        es_nulls[1].append(es_max)
    return(np.array(es_nulls[0]), np.array(es_nulls[1]))


def run_gsea(results, binding_sites, npermutations=10000):
    scores = binding_sites.copy()
    scores = scores - scores.mean(0)
    sorted_exon_ids = results.sort_values('estimate')['id']
    es_min, es_max = calc_enrichment_scores(sorted_exon_ids, scores)
    null_es_min, null_es_max = calc_null_enrichment_scores(scores,
                                                           n=npermutations)
    p = 2 * np.min(np.vstack([np.mean(es_min < null_es_min, 0),
                              np.mean(es_max > null_es_max, 0)]), axis=0)

    for i in range(p.shape[0]):
        if np.abs(es_min[i]) > np.abs(es_max[i]):
            estimate = es_min[i]
        else:
            estimate = es_max[i]

        record = {'rbp': i, 'estimate': estimate, 'pvalue': min(p[i], 1)}
        yield(record)


if __name__ == '__main__':
    log = LogTrack()
    log.write('Start GSEA analyses...')
    config = pd.read_csv(CONFIG_FPATH)
    dfs = []

    for dataset_id in config['dataset_id']:
        log.write('Analysing dataset {}'.format(dataset_id))

        log.write('Loading data...')
        fpath = join(SIMULATIONS_DIR,
                     '{}.binding_sites.csv'.format(dataset_id))
        binding_sites = pd.read_csv(fpath, index_col=0).as_matrix()

        fpath = join(SIMULATIONS_DIR, '{}.dAS.csv'.format(dataset_id))
        results = pd.read_csv(fpath)

        res = run_gsea(results, binding_sites, npermutations=10000)
        df = to_df(res)
        df['dataset_id'] = dataset_id
        dfs.append(df)
        nans = np.isnan(df['pvalue']).sum()
        if nans > 0:
            msg = '\t{} tests out of {} could not be calculated'
            log.write(msg.format(nans, df.shape[0]))

    df = pd.concat(dfs)
    fpath = join(RESULTS_DIR, 'merged_gsea.csv'.format(dataset_id))
    df.to_csv(fpath)
    log.write('Finished succesfully')
