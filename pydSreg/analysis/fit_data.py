from _collections import defaultdict
from os.path import join
from sys import argv

import numpy as np
from pydSreg.modeling.models import BayesianModel
from pydSreg.settings import SIMULATIONS_DIR, DATA_DIR
from pydSreg.utils import EventsCounts, LogTrack


def load_samples_runs(fpath):
    samples_runs = defaultdict(list)
    for line in open(fpath):
        items = line.strip().split()
        samples_runs[items[1]].append(items[0])
    return(samples_runs)


if __name__ == '__main__':
    log = LogTrack()
    log.write('Start real data analysis...')
    bs_fpath = join(DATA_DIR)

    label, model_label, dataset = argv[1], argv[2], argv[3]

    samples_runs = None
    if dataset == 'cm_maturation':
        fpath = join(DATA_DIR, dataset)
        samples_runs = load_samples_runs(fpath)

    event_type = label.capitalize().replace('_', ' ')

    log.write('Loading event counts...')
    data = EventsCounts(prefix=dataset, samples_runs=samples_runs)
    log.write('Loaded {} events wtih {} samples'.format(data.n_events,
                                                        data.n_samples))
    data.filter_event_type(event_type)
    log.write('Filtered {} {} events'.format(data.n_events, event_type))
    data.filter_counts(min_counts=1, n_samples=data.n_samples)
    log.write('Filtered {} events with at least 1 count'.format(data.n_events))
    data.filter_counts(min_counts=1, n_samples=1, label='skipping')
    data.filter_counts(min_counts=1, n_samples=1, label='inclusion')
    msg = 'Filtered {} events with 1 inclusion and skipping read'
    log.write(msg.format(data.n_events))

    fpath = join(DATA_DIR, label)
    data.load_binding_sites(fpath)
    log.write('Loaded binding sites for {} RBPs'.format(data.n_binding_sites))
    if label == 'exon_cassette':
        regions = ['Upstream_exon.l.50', 'Downstream_exon.r.50']
        regions = ['Upstream_intron.r.250', 'Downstream_intron.l.250']
        data.select_regions(regions, remove=False)

    log.write('Filtered {} RBPs'.format(data.n_binding_sites))
    X = data.get_stan_data()
    X['mappability'] = np.log(2)

    # Write counts for later analysis
    outprefix = join(DATA_DIR, '{}.{}'.format(dataset, label))
    data.counts['inclusion'].to_csv('{}.inclusion.csv'.format(outprefix))
    data.counts['total'].to_csv('{}.total.csv'.format(outprefix))
    data.binding_sites.to_csv('{}.binding_sites.csv'.format(outprefix))

    log.write('Loading model')
    pars = ['reg_effects', 'sigma', 'alpha', 'beta']
    model = BayesianModel()
    model.init(model_label=model_label)
    log.write('Start fitting...')
    model.fit(X, pars=pars, n_iter=2000, n_chains=4)
    prefix = join(SIMULATIONS_DIR, '{}.{}.{}'.format(dataset, label,
                                                     model_label))
    model.write_fit(prefix)

    log.finish()
