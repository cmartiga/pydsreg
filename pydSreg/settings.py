from os.path import join, abspath, dirname

BASE_DIR = abspath(join(dirname(__file__)))
ANALYSIS_DIR = join(BASE_DIR, 'analysis')
DATA_DIR = join(BASE_DIR, '..', 'data')
RESULTS_DIR = join(BASE_DIR, '..', 'results')
MODELING_DIR = join(BASE_DIR, 'modeling')
SIMULATIONS_DIR = join(BASE_DIR, '..', 'simulations')
PLOTS_DIR = join(ANALYSIS_DIR, 'plots', 'plots')
MODELS_DIR = join(MODELING_DIR, 'models')
CODE_DIR = join(MODELING_DIR, 'stan_code')
MODELS_LABELS = {}

CONFIG_FPATH = join(SIMULATIONS_DIR, 'config.csv')
AVAILABLE_MODELS = ['model_reg']
