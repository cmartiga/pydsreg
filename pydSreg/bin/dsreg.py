#!/usr/bin/env python

import argparse

import numpy as np
from pydSreg.modeling.models import BayesianModel
from pydSreg.settings import AVAILABLE_MODELS
from pydSreg.utils import LogTrack, EventsCounts


if __name__ == '__main__':
    description = 'dSreg performs simulatenous analysis of differential'
    description += 'inclusion and underlying regulatory mechanisms '
    description += 'using a bayesian model to integrate both layers '
    description += 'in a unique inferential step'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-i', '--inclusion', required=True,
                             help='Inclusion counts matrix')
    input_group.add_argument('-t', '--total', required=True,
                             help='Total counts matrix')
    input_group.add_argument('-b', '--binding_sites', required=True,
                             help='Event binding sites matrix')
    input_group.add_argument('-d', '--design', required=True,
                             help='Matrix containing experimental design')
    input_group.add_argument('-r', '--sel_regions', default=None,
                             help='File containing regions to filter')

    filter_group = parser.add_argument_group('Filtering')
    help_msg = 'Minimum number of countsto consider an event (1)'
    filter_group.add_argument('-m', '--min_counts', default=1, help=help_msg)
    help_msg = 'Minimum number of inclusion counts in at least 1 sample (1)'
    filter_group.add_argument('-mi', '--min_inc_counts', default=1,
                              help=help_msg)
    help_msg = 'Minimum number of skipping counts in at least 1 sample (1)'
    filter_group.add_argument('-ms', '--min_skp_counts', default=1,
                              help=help_msg)

    options_group = parser.add_argument_group('Options')
    help_msg = 'Model to fit (model_reg). Models available: {}'
    options_group.add_argument('-M', '--model', default='model_reg',
                               help=help_msg.format(AVAILABLE_MODELS))
    options_group.add_argument('-n', '--nthreads', default=1,
                               help='Number of threads to fit the model (1)')
    options_group.add_argument('-c', '--nchains', default=4,
                               help='Number MCMC chains to fit the model (4)')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', default='output.fit',
                              help='Prefix for output files (output.fit)')

    # Parse arguments
    parsed_args = parser.parse_args()
    inc_fpath = parsed_args.inclusion
    total_fpath = parsed_args.total
    binding_sites_fpath = parsed_args.binding_sites
    design_fpath = parsed_args.design
    sel_regions_fpath = parsed_args.sel_regions

    min_counts = parsed_args.min_counts
    min_inc_counts = parsed_args.min_inc_counts
    min_skp_counts = parsed_args.min_skp_counts
    model_label = parsed_args.model
    nthreads = parsed_args.nthreads
    nchains = parsed_args.nchains
    output_prefix = parsed_args.output_prefix

    # Check arguments
    if model_label not in AVAILABLE_MODELS:
        raise ValueError('{} model is not available'.format(model_label))

    # Init log
    log = LogTrack()
    log.write('Start data analysis...')

    data = EventsCounts(inc_fpath=inc_fpath, total_fpath=total_fpath)
    log.write('Loaded {} events wtih {} samples'.format(data.n_events,
                                                        data.n_samples))
    data.filter_counts(min_counts=min_counts, n_samples=data.n_samples)
    log.write('Filtered {} events with at least {} count'.format(data.n_events,
                                                                 min_counts))
    data.filter_counts(min_counts=min_inc_counts, n_samples=1,
                       label='skipping')
    data.filter_counts(min_counts=min_skp_counts, n_samples=1,
                       label='inclusion')
    msg = 'Filtered {} events with at least {} inclusion and {} skipping read'
    log.write(msg.format(data.n_events, min_inc_counts, min_skp_counts))
    data.load_binding_sites(binding_sites_fpath, use_event_id=False)
    log.write('Loaded binding sites for {} RBPs'.format(data.n_binding_sites))

    if sel_regions_fpath is not None:
        sel_regions = [line.strip() for line in open(sel_regions_fpath)]
        data.select_regions(sel_regions, remove=False)
        log.write('Filtered {} RBPs'.format(data.n_binding_sites))

    X = data.get_stan_data()
    X['mappability'] = np.log(2)

    # Analysis
    log.write('Loading model: {}'.format(model_label))
    pars = ['reg_effects', 'sigma', 'alpha', 'beta']
    model = BayesianModel()
    model.init(model_label=model_label)
    log.write('Start fitting...')
    model.fit(X, pars=pars, n_iter=2000, n_chains=4)
    model.write_fit(output_prefix)
    log.finish()
