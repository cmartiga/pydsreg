from sys import argv

from pydSreg.modeling.models import BayesianModel


if __name__ == '__main__':
    model_label = argv[1]
    model = BayesianModel()
    model.init(model_label=model_label)
    model.recompile()
