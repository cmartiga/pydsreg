#!/usr/bin/env python

from _pickle import dump, load
import multiprocessing
from os.path import exists, join
import sys
from time import ctime
import time

import numpy as np
import pandas as pd
from pydSreg.settings import DATA_DIR


class LogTrack(object):
    '''Logger class'''
    def __init__(self, fhand=None):
        if fhand is None:
            fhand = sys.stderr
        self.fhand = fhand
        self.start_time = time.time()

    def init(self, dataset_dir, script_name):
        log_fpath = join(dataset_dir, '{}.log'.format(script_name))
        self.fhand = open(log_fpath, 'w')
        self.write('Start')

    def write(self, msg, add_time=True):
        if add_time:
            msg = '[ {} ] {}\n'.format(ctime(), msg)
        else:
            msg += '\n'
        self.fhand.write(msg)
        self.fhand.flush()

    def finish(self):
        t = time.time() - self.start_time
        msg = 'Finished succesfully. Total time elapsed: {:.2f}s'.format(t)
        self.write(msg)


def classify_splicing_event(event_id):
    if 'EX' in event_id:
        return('Exon cassette')
    elif 'ALTD' in event_id:
        return('Alternative donor')
    elif 'ALTA' in event_id:
        return('Alternative acceptor')
    elif 'INT' in event_id:
        return('Intron retention')
    return(None)


def rename_region(region):
    return(region.split('.')[0].replace('_', ' ').capitalize())


def rename_rbp(rbp):
    rbp, region = rbp.split('.', 1)
    region = rename_region(region)
    return('{}-{}'.format(rbp, region))


def add_rbp_and_region_cols(df, fieldname='rbp'):
    df['rbp_name'] = [x.split('.', 1)[0] for x in df[fieldname]]
    df['region'] = [rename_region(x.split('.', 1)[1]) for x in df[fieldname]]


def load_calc_results(fpath, funct, args, force=False):
    '''Function to load generally pre-computed data if the given
       path exists'''

    if not force and exists(fpath):
        with open(fpath, 'rb') as fhand:
            results = load(fhand)
    else:
        results = funct(*args)
        with open(fpath, 'wb') as fhand:
            dump(results, fhand)
    return results


def run_parallel(func, iterable, threads=None, chunks=False):
    if threads is None:
        map_func = map
    else:
        workers = multiprocessing.Pool(threads)
        if chunks:
            map_func = workers.starmap
        else:
            map_func = workers.map
    return map_func(func, iterable)


def column_groupby(df, design, groupby, function=np.median):
    data = {}
    design['id'] = design.index
    for key, values in design.groupby(groupby)['id']:
        data[key] = function(df[values], axis=1)
    return(pd.DataFrame(data))


def splitby_df(df, design, splitby):
    split_data = {}
    design['id'] = design.index
    for key, values in design.groupby(splitby)['id']:
        colnames = np.intersect1d(values, df.columns)
        if colnames.shape[0] > 0:
            split_data[key] = df[colnames]
    return(split_data)


def load_pickle(fpath):
    with open(fpath, 'rb') as fhand:
        data = load(fhand)
    return(data)


def write_pickle(data, fpath):
    with open(fpath, 'wb') as fhand:
        dump(data, fhand)
    return(data)


def calc_calibration(ci, values):
    if ci is None:
        calibration = None
    else:
        calibration = np.mean(np.logical_and(ci[0] < values, ci[1] > values))
    return(calibration)


class EventsCounts(object):
    def __init__(self, prefix=None, samples_runs=None, inc_fpath=None,
                 total_fpath=None, events_fpath=None):
        self.load_counts(prefix=prefix, samples_runs=samples_runs,
                         inc_fpath=inc_fpath, total_fpath=total_fpath,
                         events_fpath=events_fpath)
        self.prefix = prefix

    @property
    def n_samples(self):
        return(self.counts['total'].shape[1])

    @property
    def n_events(self):
        return(self.counts['total'].shape[0])

    @property
    def n_binding_sites(self):
        if hasattr(self, 'binding_sites'):
            return(self.binding_sites.shape[1])
        else:
            return(0)

    def get_fpaths(self, prefix=None, inc_fpath=None, total_fpath=None,
                   events_fpath=None):

        fpaths = {}
        if prefix is not None:
            for suffix in ['inclusion', 'total', 'events']:
                fpath = join(DATA_DIR, '{}.{}.csv'.format(prefix, suffix))
                if not exists(fpath):
                    if suffix != 'events':
                        raise ValueError('{} file not found'.format(fpath))
                    else:
                        continue
                fpaths[suffix] = fpath
        else:
            if inc_fpath is None or total_fpath is None:
                msg = 'Either a prefix of files for inclusion and skipping'
                msg += ' must be provided'
                raise ValueError(msg)
            if not exists(inc_fpath) or not exists(total_fpath):
                msg = '{} or {} files not found'.format(inc_fpath, total_fpath)
                raise ValueError(msg)

            fpaths['inclusion'] = inc_fpath
            fpaths['total'] = total_fpath
            if events_fpath is not None:
                if not exists(events_fpath):
                    raise Warning('{} file not found. Ignoring event data')
                fpaths['events'] = total_fpath
        return(fpaths)

    def load_counts(self, prefix=None, samples_runs=None, inc_fpath=None,
                    total_fpath=None, events_fpath=None):
        counts = {}
        fpaths = self.get_fpaths(prefix=prefix, inc_fpath=inc_fpath,
                                 total_fpath=total_fpath,
                                 events_fpath=events_fpath)
        for suffix, fpath in fpaths.items():
            df = pd.read_csv(fpath, index_col=0).fillna(0)
            if suffix != 'events' and samples_runs is not None:
                df = pd.DataFrame({sample: df[runs].sum(1)
                                   for sample, runs in samples_runs.items()})
            counts[suffix] = df

        counts['skipping'] = counts['total'] - counts['inclusion']
        if 'events' in counts:
            counts['events']['type'] = [classify_splicing_event(event_id)
                                        for event_id in counts['events'].index]
            for suffix in ['inclusion', 'total']:
                counts['events'][suffix] = counts[suffix].mean(1)
        self.counts = counts

    def filter_rows(self, sel_rows):
        filtered = {}
        for key, value in self.counts.items():
            filtered[key] = value.loc[sel_rows, :]
        self.counts = filtered

    def filter_counts(self, min_counts, n_samples=1, label='total'):
        sel_rows = (self.counts[label] > min_counts).sum(1) >= n_samples
        self.filter_rows(sel_rows)

    def filter_event_type(self, event_type):
        sel_rows = self.counts['events']['type'] == event_type
        self.filter_rows(sel_rows)

    def load_binding_sites(self, fpath, use_event_id=True):
        if use_event_id:
            binding_sites = pd.read_csv(fpath).set_index('event_id')
        else:
            binding_sites = pd.read_csv(fpath, index_col=0)

        binding_sites = binding_sites.loc[self.counts['total'].index, :]
        binding_sites = (binding_sites > 0).astype(int)
        self.binding_sites = binding_sites

    def select_regions(self, regions, remove=False):
        sel_cols = [x for x in self.binding_sites.columns
                    if x.split('.', 1)[-1] in regions != remove]
        self.binding_sites = self.binding_sites.loc[:, sel_cols]

    def get_stan_data(self, design=None):
        if design is None:
            design = np.array([0] * int(self.n_samples / 2) + [1] * int(self.n_samples / 2))
        data = {'inclusion': self.counts['inclusion'].transpose().astype(int),
                'total': self.counts['total'].transpose().astype(int)}
        data['binding_sites'] = self.binding_sites
        data['W'] = self.n_binding_sites
        data['K'] = self.n_samples
        data['N'] = self.n_events
        data['design'] = design
        return(data)
