#!/usr/bin/env python
from pandas.util.testing import isiterable
from seaborn.distributions import _scipy_univariate_kde, \
    _statsmodels_univariate_kde

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


# Define palettes
current_palette = sns.color_palette('Set1')

CLASS_COLORS = {'FDR<0.2': current_palette[0],
                'FDR<0.05': current_palette[1],
                'GLM': current_palette[1],
                'ORA': current_palette[1],
                'Null model': current_palette[2],
                'dSreg': current_palette[4],
                'GSEA': current_palette[2]}

TYPES_COLORS = {'Event': 'lightblue',
                'Regulator': 'm'}

GROUPS_PAL = {'Included': 'blue',
              'Skipped': 'red'}
REGIONS_PAL = {'Upstream intron': 'purple',
               'Downstream intron': 'violet'}


def replace_class_labels(series, thresholds=[0.2, 0.1, 0.05]):
    for threshold in thresholds:
        series.replace('fdr{}'.format(threshold),
                       'FDR<{}'.format(threshold), inplace=True)

    series.replace('Class.raw', 'Nominal', inplace=True)
    series.replace('Class.adj', 'FDR', inplace=True)

    series.replace('model', 'Null model', inplace=True)
    series.replace('HS', 'dSreg', inplace=True)


# Functions
def init_fig(nrow=1, ncol=1, figsize=None, style='white',
             colsize=3, rowsize=3):
    sns.set_style(style)
    if figsize is None:
        figsize = (colsize * ncol, rowsize * nrow)
    fig, axes = plt.subplots(nrow, ncol, figsize=figsize)
    return(fig, axes)


def savefig(fig, fpath):
    fig.tight_layout()
    fig.savefig(fpath, format=fpath.split('.')[-1], dpi=180)
    plt.close()


def create_patches_legend(axes, colors_dict, loc=1, **kwargs):
    axes.legend(handles=[mpatches.Patch(color=color, label=label)
                         for label, color in sorted(colors_dict.items())],
                loc=loc, **kwargs)


def plot_PCA(design_pca, explained_variance, species, fpath):
    fig, subplots = init_fig(1, 2, colsize=6, rowsize=6)

    axes = subplots[0]
    palette = sns.color_palette(n_colors=len(species))
    for sp, color in zip(species, palette):
        df = design_pca.loc[design_pca['organism'] == sp, :]
        label = sp.replace('_', ' ').capitalize()
        axes.scatter(df['PC1'], df['PC2'], c=color, label=label)
    axes.set_xlabel('PC1 ({:.3} %)'.format(explained_variance[0] * 100))
    axes.set_ylabel('PC2 ({:.3} %)'.format(explained_variance[1] * 100))
    axes.legend(loc=1, fancybox=True, frameon=True, ncol=2)

    axes = subplots[1]
    tissues = design_pca['tissue'].unique()
    palette = sns.color_palette(n_colors=len(tissues))
    for ts, color in zip(tissues, palette):
        label = ts.replace('_', ' ').capitalize()
        df = design_pca.loc[design_pca['tissue'] == ts, :]
        axes.scatter(df['PC1'], df['PC2'], c=color, label=label)
    axes.set_xlabel('PC1 ({:.3} %)'.format(explained_variance[0] * 100))
    axes.set_ylabel('PC2 ({:.3} %)'.format(explained_variance[1] * 100))
    axes.legend(loc=1, fancybox=True, frameon=True, ncol=2)

    sns.despine()
    savefig(fig, fpath)


def plot_tpms_means_vars(medians, stds, fpath, palette=None):
    fig, subplots = init_fig(1, 2, rowsize=4, colsize=4)

    axes = subplots[0]
    colors = sns.color_palette(palette, n_colors=medians.shape[1])
    for ts, color in zip(medians.columns, colors):
        label = ts.capitalize().replace('_', ' ')
        sns.kdeplot(medians.loc[medians[ts] > -5, ts], ax=axes,
                    color=color, label=label)
    axes.legend(loc=2, frameon=True, fancybox=True)
    axes.set_xlabel('log(TPMs+1)')
    axes.set_ylabel('Density')
    axes.set_xlim((-15, 10))
    axes.set_yticklabels([])

    axes = subplots[1]
    n = medians.shape[0] * medians.shape[1]
    sel_idxs = np.random.randint(0, n, size=2000)
    medians_melt = pd.melt(medians).iloc[sel_idxs, :]
    stds_melt = pd.melt(stds).iloc[sel_idxs, :]
    sel_idxs = np.logical_and(np.logical_not(np.isnan(medians_melt['value'])),
                              np.logical_not(np.isnan(stds_melt['value'])))
    medians_melt = medians_melt.loc[sel_idxs, :]
    stds_melt = stds_melt.loc[sel_idxs, :]
    sns.kdeplot(medians_melt['value'], stds_melt['value'],
                cmap='Blues', ax=axes)
    axes.set_xlabel(r'$log(TPMs+1)_{mean}$')
    axes.set_ylabel(r'$log(TPMs+1)_{sd}$')
    axes.set_xlim((-5, 10))
    axes.set_ylim((0, 2.5))

    sns.despine()
    savefig(fig, fpath)


def plot_evaluation_sim(param, target, df, fpath, xlabel=None, ylabel=None):
    if xlabel is None:
        xlabel = param
    if ylabel is None:
        ylabel = target

    fig, subplots = init_fig(1, 2, colsize=3.5)
    axes = subplots[0]
    axes = sns.barplot(x='value', y='converged', data=df, color='grey',
                       linewidth=1.5, ax=axes, edgecolor='black', n_boot=0)
    axes.set_xlabel(xlabel)
    axes.set_ylabel('Converged')

    axes = subplots[1]
    converged = df.loc[df['converged'], :]
    if converged.shape[0] > 0:
        axes = sns.boxplot(x='value', y=target, data=converged,
                           showfliers=False, ax=axes, color='white')
        axes.set_xlabel('Number of genes')
        axes.set_ylabel('Reduced distances by fitting')

        if xlabel:
            axes.set_xlabel(xlabel)
        if ylabel:
            axes.set_ylabel(ylabel)

    sns.despine()
    savefig(fig, fpath)


def arrange_plot(axes, xlims=None, ylims=None, xlabel=None, ylabel=None,
                 showlegend=False, legend_loc=None, hline=None,
                 rotate_xlabels=False, cols_legend=1, rotation=90,
                 legend_frame=False, title=None, ticklabels_size=None,
                 yticklines=False, fontsize=None, vline=None, despine=True,
                 despine_left=False, despine_bottom=False):
    if xlims is not None:
        axes.set_xlim(xlims)
    if ylims is not None:
        axes.set_ylim(ylims)
    if title is not None:
        axes.set_title(title)

    if xlabel is not None:
        axes.set_xlabel(xlabel, fontsize=fontsize)
    if ylabel is not None:
        axes.set_ylabel(ylabel, fontsize=fontsize)

    if showlegend:
        axes.legend(loc=legend_loc, ncol=cols_legend,
                    frameon=legend_frame, fancybox=legend_frame)
    elif axes.legend_ is not None:
        axes.legend_.set_visible(False)

    if hline is not None:
        xlims = axes.get_xlim()
        axes.plot(xlims, (hline, hline), linewidth=1, color='grey',
                  linestyle='--')
        axes.set_xlim(xlims)

    if vline is not None:
        if not isiterable(vline):
            vline = [vline]
        ylims = axes.get_ylim()
        for x in vline:
            axes.plot((x, x), ylims, linewidth=0.7, color='grey',
                      linestyle='--')
        axes.set_ylim(ylims)

    if rotate_xlabels:
        axes.set_xticklabels(axes.get_xticklabels(), rotation=rotation)
    if ticklabels_size is not None:
        for tick in axes.xaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
        for tick in axes.yaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
    if yticklines:
        xlims = axes.get_xlim()
        for y in axes.get_yticks():
            axes.plot(xlims, (y, y), lw=0.4, alpha=0.2, c='grey')

    if despine:
        sns.despine(ax=axes, left=despine_left, bottom=despine_bottom)


def barplot(data, axes, x, y, hue=None, xlabel=None, ylabel=None,
            showlegend=True, legend_loc=1, xlims=None, ylims=None, hline=None,
            rotate_xlabels=False, hue_order=None):

    axes = sns.barplot(x=x, y=y, hue=hue, data=data,
                       ax=axes, n_boot=0, linewidth=1.5,
                       edgecolor='black', hue_order=hue_order)
    _arrange_plot(axes, xlims, ylims, xlabel, ylabel, showlegend, legend_loc,
                  hline, rotate_xlabels)


def boxplot(data, axes, x, y, hue=None, xlabel=None, ylabel=None,
            showlegend=True, legend_loc=1, xlims=None, ylims=None, hline=None,
            rotate_xlabels=False, cols_legend=3, hue_order=None):

    axes = sns.boxplot(x=x, y=y, hue=hue, data=data,
                       ax=axes, showfliers=False, hue_order=hue_order)
    _arrange_plot(axes, xlims, ylims, xlabel, ylabel, showlegend, legend_loc,
                  hline, rotate_xlabels, cols_legend)


def _plot_post_pred_ax(x, q, axes, color):
    axes.fill_between(x, q[0, :], q[-1, :], facecolor=color,
                      interpolate=True, alpha=0.1)
    axes.fill_between(x, q[1, :], q[-2, :], facecolor=color,
                      interpolate=True, alpha=0.1)
    axes.fill_between(x, q[2, :], q[-3, :], facecolor=color,
                      interpolate=True, alpha=0.1)
    axes.fill_between(x, q[3, :], q[-4, :], facecolor=color,
                      interpolate=True, alpha=0.1)
    axes.fill_between(x, q[4, :], q[-5, :], facecolor=color,
                      interpolate=True, alpha=0.2)
    axes.plot(x, q[5, :], color=color, linewidth=2)


def plot_post_pred(pred_props, fpath, times=None, labels=None,
                   xs=None, ys=None, overlay_plots=False):
    percs = [2.5, 5, 10, 25, 40, 50, 60, 75, 90, 95, 97.5]
    q = np.percentile(pred_props, q=percs, axis=0)
    if times is None:
        times = np.arange(q.shape[1])
    n_cell_types = pred_props.shape[2]

    if overlay_plots:
        fig, subplots = init_fig(1, 1, colsize=4, rowsize=2.5)
        subplots = np.array([subplots] * n_cell_types)
    else:
        fig, subplots = init_fig(int(n_cell_types / 2), 2, colsize=4,
                                 rowsize=2.5)
    subplots = subplots.flatten()
    palette = sns.color_palette(palette='rainbow', n_colors=n_cell_types)
    if labels is None:
        labels = ['Posterior predictive {}'.format(i)
                  for i in range(n_cell_types)]

    for i, (color, axes) in enumerate(zip(palette, subplots)):
        _plot_post_pred_ax(times, q[:, :, i], axes, color)
        if xs is not None and ys is not None:
            axes.scatter(xs, ys[:, i], color=color)

        axes.set_ylabel(labels[i])
        axes.set_ylim((0, 1))
        axes.set_xlabel('')
#         axes.set_xticklabels([])

    axes.set_xlabel('Time')
    axes = subplots[-2]
    axes.set_xlabel('Time')

    sns.despine()
    savefig(fig, fpath)


def add_panel_label(axes, label, fontsize=20, yfactor=0.03, xfactor=0.215):
    xlims, ylims = axes.get_xlim(), axes.get_ylim()
    x = xlims[0] - (xlims[1] - xlims[0]) * xfactor
    y = ylims[1] + (ylims[1] - ylims[0]) * yfactor
    axes.text(x, y, label, fontsize=fontsize)


def plot_traces(traces, params, fpath):
    fig, subplots = init_fig(nrow=len(params), ncol=2, colsize=4,
                             rowsize=2, style='white')
    for i, param in enumerate(params):
        sample = np.array(traces[param])
        if len(set(sample)) > 1:
            sns.kdeplot(sample, shade=False, ax=subplots[i][0])

        x = np.arange(sample.shape[0])
        subplots[i][1].plot(x, sample,
                            linewidth=0.2)
        subplots[i][0].set_ylabel('Density')
        subplots[i][0].set_xlabel(param)
        subplots[i][1].set_xlabel('Iteration')
        subplots[i][1].set_ylabel(param)
    savefig(fig, fpath)


def joyplots(x, y, data, fig, gs, gs_xs, gs_ys, hue=None, yoverlap=1,
             palette=None, color='purple', alpha=1, showlabels=True,
             label_xfactor=0, label_yfactor=0.3, showlegend=True,
             xlabel=None, ylabel=None, panel_label=None, panel_label_xfactor=0,
             whiteline=True, lw=1.2, groups=None, xlims=None,
             xticks=None, title=None):
    if groups is None:
        groups = data[y].unique()
    n_axes = groups.shape[0]
    total_size = gs_ys[1] - gs_ys[0]
    axes_h = (total_size + yoverlap * (n_axes - 1)) / n_axes

    if palette is None and hue is not None:
        hue_values = data[hue].unique()
        palette = dict(zip(hue_values,
                           sns.color_palette(n_colors=hue_values.shape[0])))

    ystart = gs_ys[0]
    all_xlims = None
    axes_list = []
    for label in groups:
        yend = int(ystart + axes_h) - 1
        try:
            axes = fig.add_subplot(gs[ystart:yend, gs_xs[0]:gs_xs[1]])
        except IndexError:
            axes = fig.add_subplot(gs[ystart:yend - 1, gs_xs[0]:gs_xs[1]])
        ystart = yend - yoverlap

        if hue is None:
            xs = data.loc[data[y] == label, x].dropna()
            if xs.shape[0] > 2:
                sns.kdeplot(xs, shade=True, ax=axes, alpha=alpha, color=color,
                            lw=0.5)
                if whiteline:
                    sns.kdeplot(xs, shade=False, ax=axes, color='white',
                                lw=lw, label='_nolegend_', legend=False)
            ymax = axes.get_ylim()[1]
        else:
            ymax = 0
            for group, color in palette.items():
                sel_rows = np.logical_and(data[y] == label, data[hue] == group)
                xs = data.loc[sel_rows, x].dropna()
                if xs.shape[0] > 2:
                    sns.kdeplot(xs, shade=True, ax=axes, alpha=alpha, color=color,
                                label=group, lw=0)
                    if whiteline:
                        sns.kdeplot(xs, shade=False, ax=axes, color='white',
                                    lw=lw, label='_nolegend_', legend=False)
                    ymax = max(ymax, max(_statsmodels_univariate_kde(np.array(xs), kernel="gau", bw="scott", gridsize=100, cut=3, clip=[-0.3, 0.3])[1]))

        if xlims is None:
            xlims = axes.get_xlim()
        if ymax == 0:
            ymax = axes.get_ylim()
        ylims = (0, 1.1 * ymax)
        axes.set_ylim(ylims)
        axes.set_xlabel('')
        axes.set_ylabel('')
        axes.set_xticklabels([])
        axes.set_yticklabels([])

        if axes.legend_ is not None:
            axes.legend_.set_visible(False)
        if all_xlims is None:
            all_xlims = xlims
        else:
            all_xlims = (min(all_xlims[0], xlims[0]),
                         max(all_xlims[1], xlims[1]))
        sns.despine(ax=axes, left=True)
        axes_list.append(axes)

    for axes, label in zip(axes_list, groups):
        axes.plot(all_xlims, (0, 0), lw=1.5, color='black')
        axes.set_xlim(all_xlims)
        print(all_xlims)
        ylims = axes.get_ylim()
        if showlabels:
            x = all_xlims[0] + label_xfactor * (all_xlims[1] - all_xlims[0])
            y = ylims[0] + label_yfactor * (ylims[1] - ylims[0])
            axes.text(x, y, label, fontsize=9)

    if xticks is not None:
        axes_list[-1].set_xticks(xticks)
    else:
        xticks = axes_list[-1].get_xticks()
    axes_list[-1].set_xticklabels(['{:.1f}'.format(x) for x in xticks],
                                  fontsize=7)
    if showlegend and hue is not None:
        create_patches_legend(axes_list[0], palette, loc=(0, 1), ncol=2,
                              fontsize=8)
    if xlabel is not None:
        axes_list[-1].set_xlabel(xlabel)
    if ylabel is not None:
        axes_list[int(len(axes_list) / 2)].set_ylabel(ylabel)
    if title is not None:
        axes_list[0].set_title(title, fontsize=7)
    if panel_label is not None:
        add_panel_label(axes_list[0], panel_label, xfactor=panel_label_xfactor)


def add_edge(axes, begin, end, target_size=None, arrow_size=0.15,
             alpha=1, arrow_type='->', linestyle='-'):
    if end[0] == begin[0]:
        slope = (end[1] - begin[1]) / 10e-5
    else:
        slope = (end[1] - begin[1]) / (end[0] - begin[0])
    arrow_end = end
    if target_size is not None:
        dx = (0.1 + target_size) ** 2 / np.sqrt(1 + slope ** 2)
        dy = slope * dx
        if begin[0] - end[0] > 0:
            arrow_end = (arrow_end[0] + dx, arrow_end[1] + dy)
        else:
            arrow_end = (arrow_end[0] - dx, arrow_end[1] - dy)

    if slope == 0:
        ortho_slope = (end[0] - begin[0]) * 10e5
    else:
        ortho_slope = -1 / slope

    if arrow_type == '->':
        dx = arrow_size ** 2 / np.sqrt(1 + slope ** 2)
        dy = slope * dx

        if begin[0] - end[0] > 0:
            arrow_start = (arrow_end[0] + dx, arrow_end[1] + dy)
        else:
            arrow_start = (arrow_end[0] - dx, arrow_end[1] - dy)

        axes.plot((begin[0], arrow_start[0]), (begin[1], arrow_start[1]), alpha=alpha,
                  color='black', linewidth=0.7, zorder=1, linestyle=linestyle)

        dx = (arrow_size / 2) ** 2 / np.sqrt(1 + ortho_slope ** 2)
        dy = ortho_slope * dx
        start1 = (arrow_start[0] + dx, arrow_start[1] + dy)
        start2 = (arrow_start[0] - dx, arrow_start[1] - dy)
        arrow = plt.Polygon([start1, start2, arrow_end], alpha=alpha,
                            color='black', zorder=1)
        plt.gca().add_patch(arrow)

    elif arrow_type == '-|':
        dx = (arrow_size * 0.75) ** 2 / np.sqrt(1 + slope ** 2)
        dy = slope * dx

        if begin[0] - end[0] > 0:
            arrow_start = (arrow_end[0] + dx, arrow_end[1] + dy)
        else:
            arrow_start = (arrow_end[0] - dx, arrow_end[1] - dy)

        axes.plot((begin[0], arrow_start[0]), (begin[1], arrow_start[1]),
                  alpha=alpha, color='black', linewidth=0.7, zorder=1,
                  linestyle=linestyle)
        dx = (arrow_size / 2) ** 2 / np.sqrt(1 + ortho_slope ** 2)
        dy = ortho_slope * dx
        start1 = (arrow_start[0] + dx, arrow_start[1] + dy)
        start2 = (arrow_start[0] - dx, arrow_start[1] - dy)
        axes.plot((start1[0], start2[0]), (start1[1], start2[1]),
                  alpha=alpha, color='black', linewidth=0.7, zorder=1)

    elif arrow_type is None:
        axes.plot((begin[0], end[0]), (begin[1], end[1]),
                  alpha=alpha, color='black', linewidth=0.7, zorder=1)

    else:
        msg = 'Arrow type not supported: {}'.format(arrow_type)
        raise ValueError(msg)


def get_panel_subplots(fig, gs, nrow, ncol, xs, ys, yspace=2, xspace=2):
    subplots = []

    ysize = (ys[1] - ys[0] - yspace * (nrow - 1)) / (nrow)
    y_intervals = [[int(ys[0] + ysize * i + yspace * i),
                    int(ys[0] + ysize * i + yspace * i + ysize)]
                   for i in range(nrow)]

    xsize = (xs[1] - xs[0] - xspace * (ncol - 1)) / (ncol)
    x_intervals = [[int(xs[0] + xsize * i + xspace * i),
                    int(xs[0] + xsize * i + xspace * i + xsize)]
                   for i in range(ncol)]

    for ystart, yend in y_intervals:
        for xstart, xend in x_intervals:
            subplots.append(fig.add_subplot(gs[ystart:yend, xstart:xend]))
    return(subplots)
